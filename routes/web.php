<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

// ********* API Routes ********** //

// Authorize To Get Token
Route::resource('/api/Authorize', 'AuthenticateController', ['only'=> ['index','store']]);
// Login a User Account
Route::resource('/api/Login', 'LoginController', ['only'=> ['index','store']]);
// Register a User Account
Route::resource('/api/Register', 'RegisterController', ['only'=> ['index','store']]);
// Insert and Retrieve Users PedometerController
Route::resource('/api/Pedometer', 'PedometerController', ['only'=> ['index','store']]);
// Custom Get Route For Pedometer
Route::get('/api/Pedometer/{usr_key}', 'PedometerController@getUserSteps');
Route::get('/api/Pedometer/Today', 'PedometerController@getUserStepLogsToday');
// Insert and Retrieve Users Water Intake Info
Route::resource('/api/Water', 'PedometerController', ['only'=> ['index','store']]);
// Custom Get Route For Water Log
Route::get('/api/Water/{usr_key}', 'WaterLogController@getUserWaterLogs');
Route::get('/api/Water/Today', 'WaterLogController@getUserWaterLogsToday');
