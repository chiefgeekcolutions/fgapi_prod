<?php namespace App\CustomLib;

date_default_timezone_set("America/New_York");

use Illuminate\Support\Facades\DB;
use \Illuminate\Database\QueryException;

    use App\Http\Models\ErrorClass;
		use App\Http\Models\ErrorLog;

    class DataUtils
    {
        public static function ExecuteScalar($szSql, $parameters)
        {
						$oEr = new ErrorClass();
            try {
                // do your database transaction here
                if (!empty($parameters)) {
                    $result = DB::select($szSql, $parameters);
                    //var_dump($szSql, $parameters);
                } else {
                    $result = DB::select($szSql);
                }
            } catch (QueryException $e) {
                // something went wrong with the transaction, rollback
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("ExecuteScalar", "", "Web API", $oEr);
                $result = $oEr;
            } catch (Exception $e) {
                // something went wrong elsewhere, handle gracefully
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("ExecuteScalar", "", "Web API", $oEr);
                $result = $oEr;
            }

            return json_decode(json_encode($result), false);
        }

        public static function Update($szSql, $parameters)
        {
						$oEr = new ErrorClass();
            try {
                // do your database transaction here
                if (!empty($parameters)) {
                    $result = DB::update($szSql, $parameters);
                } else {
                    $result = DB::update($szSql);
                }
            } catch (QueryException $e) {
                // something went wrong with the transaction, rollback
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("Update", "", "Web API", $oEr);
                $result = $oEr;
            } catch (Exception $e) {
                // something went wrong elsewhere, handle gracefully
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("Update", "", "Web API", $oEr);
                $result = $oEr;
            }
            return $result;
        }

        public static function Insert($szSql, $parameters)
        {
						$oEr = new ErrorClass();
            try {
                // do your database transaction here
                if (!empty($parameters)) {
                    $result = DB::insert($szSql, $parameters);
                } else {
                    $result = DB::insert($szSql);
                }
            } catch (QueryException $e) {
                // something went wrong with the transaction, rollback
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("Insert", "", "Web API", $oEr);
                $result = $oEr;
            } catch (Exception $e) {
                // something went wrong elsewhere, handle gracefully
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("Insert", "", "Web API", $oEr);
                $result = $oEr;
            }
            return $result;
        }

        public static function simpleInsert($table, $values)
        {
            $oEr = new ErrorClass();
            try {
                // do your database transaction here
                $result = DB::table($table)->insert($values);
            } catch (QueryException $e) {
                // something went wrong with the transaction, rollback
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("simpleInsert", "", "Web API", $oEr);
                $result = $oEr;
            } catch (Exception $e) {
                // something went wrong elsewhere, handle gracefully
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("simpleInsert", "", "Web API", $oEr);
                $result = $oEr;
            }
            return $result;
        }

        public static function GeneralStatement($szSql)
        {
            $oEr = new ErrorClass();
            try {
                // do your database transaction here
                $result = DB::statement($szSql);
            } catch (QueryException $e) {
                // something went wrong with the transaction, rollback
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("GeneralStatement", "", "Web API", $oEr);
                $result = $oEr;
            } catch (Exception $e) {
                // something went wrong elsewhere, handle gracefully
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("GeneralStatement", "", "Web API", $oEr);
                $result = $oEr;
            }
            return $result;
        }

        public static function simpleSelect($table, $column, $value)
        {
					 $oEr = new ErrorClass();
            try {
                // do your database transaction here
                $result = DB::table($table)->where($column, $value)->first();
            } catch (QueryException $e) {
                // something went wrong with the transaction, rollback
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("simpleSelect", "", "Web API", $oEr);
                $result = $oEr;
            } catch (Exception $e) {
                // something went wrong elsewhere, handle gracefully
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("simpleSelect", "", "Web API", $oEr);
                $result = $oEr;
            }
            return $result;
        }

        public static function select($table, $column, $value)
        {
						$oEr = new ErrorClass();
            try {
                // do your database transaction here
                $result = DB::table($table)->where($column, $value);
            } catch (QueryException $e) {
                // something went wrong with the transaction, rollback
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("select", "", "Web API", $oEr);
                $result = $oEr;
            } catch (Exception $e) {
                // something went wrong elsewhere, handle gracefully
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("select", "", "Web API", $oEr);
                $result = $oEr;
            }
            return $result;
        }

        public static function EasyUpdate($table, $key, $keyvalue, $values)
        {
						$oEr = new ErrorClass();
            try {
                // do your database transaction here
                $result = DB::table($table)
                            ->where($key, $keyValue)
                            ->update($values);
            } catch (QueryException $e) {
                // something went wrong with the transaction, rollback
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("EasyUpdate", "", "Web API", $oEr);
                $result = $oEr;
            } catch (Exception $e) {
                // something went wrong elsewhere, handle gracefully
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("EasyUpdate", "", "Web API", $oEr);
                $result = $oEr;
            }
            return $result;
        }

				public static function selectWhere($table, $select, $where)
        {
						$oEr = new ErrorClass();
            try {
                // do your database transaction here
                $result = DB::table($table)
				                     ->select(DB::raw($select))
				                     ->where($where)
				                     ->get();
            } catch (QueryException $e) {
                // something went wrong with the transaction, rollback
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("selectWhere", "", "Web API", $oEr);
                $result = $oEr;
            } catch (Exception $e) {
                // something went wrong elsewhere, handle gracefully
                $oEr->ErrorNumber = -1;
                $oEr->Message = $e->getMessage();
								logError("selectWhere", "", "Web API", $oEr);
                $result = $oEr;
            }
            return $result;
        }

				public static logError($method, $message, $addUser, $Err) {
						// Create ErrLog Object
						$erLog = new ErrorLog();
						$erLog->err_message = $message;
						$erLog->err_method = $method;
						$erLog->err_debug_message = $Err->Message;
						$erLog->err_add_date = date("Y-m-d h:i:sa", $d);;
						$erLog->err_add_user = $addUser;
						// Insert
						simpleInsert("fw_error_log", $erLog);
				}
    }
