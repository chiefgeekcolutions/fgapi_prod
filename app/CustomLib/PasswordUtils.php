<?php namespace App\CustomLib;

use Crypt;

class PasswordUtils {

	public static function CheckPassword($raw, $encrypted) {
		if($raw == PasswordUtils::decryptPasswordForLogin($encrypted)){
			return true;
		}
		return false;
	}
  // Encrypts Password that is sent in plain text
  public static function encryptPasswordForInsert($rawPwd) {
    $encPwd = Crypt::encryptString($rawPwd);
    return $encPwd;
  }

  // Decrypts encrypted password that is sent
  public static function decryptPasswordForLogin($encPassword){
    $decPwd = Crypt::decryptString($encPassword);
    return $decPwd;
  }
}

 ?>
