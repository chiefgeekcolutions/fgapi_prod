<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

// Models
use App\Http\Models\ErrorClass;
use App\Http\Models\Authentication;
use App\Http\Models\LoginModel;

// Utilities
use App\CustomLib\DataUtils;
use App\CustomLib\PasswordUtils;
use App\CustomLib\AuthUtils;

class WaterLogController extends BaseController
{
    public function index()
    {
        $returnVal = ['value_1' => '1',
            'value_2' => '2'];

        //
        return response()->json(
                    $returnVal,
                    200,
                ['Content-type'=> 'application/json; charset=utf-8'],
                JSON_PRETTY_PRINT
                );
    }

    public function getUserWaterLogs($usr_key)
    {
        // Select All from Pedometer
        $result = DataUtils::select('lg_pedometer_lo', 'ped_usr_key', $usr_key);

        return response()->json(
                $result,
                200,
            ['Content-type'=> 'application/json; charset=utf-8'],
            JSON_UNESCAPED_UNICODE
            );
    }

    public function getUserWaterLogsToday(Request $request)
    {
        $oEr = new ErrorClass();
        $response = "";

        $szAuthToken = $request->header("token");
        $authResult = AuthUtils::Authenticate($szAuthToken);

        $oWaterLog = $request->all();

        if ($authResult) {
            $szSql = sprintf(
                        "select * from lg_water_log where wtr_key = '%s' and wtr_start_date = '%s' and wtr_end_date = '%s'",
                        $oWaterLog->wtr_key,
                        $oWaterLog->wtr_start_date,
                        $oWaterLog->wtr_end_date
                    );
            $response = DataUtils::ExecuteScalar($szSql, null);
        } else {
            $oEr->ErrorNumber = -1;
            $oEr->Message = "Authentication failed, please reauthenticate and try again.";
            $response = $oEr;
        }
        return response()->json(
                            $response,
                            200,
                    ['Content-type'=> 'application/json; charset=utf-8'],
                    JSON_PRETTY_PRINT
                    );
    }

    public function store(Request $request)
    {
        $oEr = new ErrorClass();
        $response = "";

        $szAuthToken = $request->header("token");
        $authResult = AuthUtils::Authenticate($szAuthToken);

        if ($authResult) {
            $userInput = $request->all();
            //print_r($input);
            $response = DataUtils::simpleInsert("lg_pedometer_log", $userInput);
        } else {
            $oEr->ErrorNumber = -1;
            $oEr->Message = "Authentication failed, please reauthenticate and try again.";
            $response = $oEr;
        }

        return response()->json(
                $response,
                200,
            ['Content-type'=> 'application/json; charset=utf-8'],
            JSON_PRETTY_PRINT
            );
    }

    public function updateLog(Request $request)
    {
        $oEr = new ErrorClass();
        $response = "";

        $szAuthToken = $request->header("token");
        $authResult = AuthUtils::Authenticate($szAuthToken);

        if ($authResult) {
            $userInput = $request->all();
            //print_r($input);
            $response = DataUtils::EasyUpdate("lg_pedometer_log", 'ped_key', $request->ped_key, $userInput);
        } else {
            $oEr->ErrorNumber = -1;
            $oEr->Message = "Authentication failed, please reauthenticate and try again.";
            $response = $oEr;
        }

        return response()->json(
                $response,
                200,
            ['Content-type'=> 'application/json; charset=utf-8'],
            JSON_UNESCAPED_UNICODE
            );
    }
}
