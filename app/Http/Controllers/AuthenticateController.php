<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

// Models
use App\Http\Models\ErrorClass;
use App\Http\Models\Authentication;
use App\Http\Models\SecurityUser;

// Utilities
use App\CustomLib\DataUtils;
use App\CustomLib\PasswordUtils;

class test
{
    public $username;
    public $password;
}

class AuthenticateController extends BaseController
{
    public function index()
    {
        $returnVal = ['value_1' => '1',
      'value_2' => '2'];
        //
        return response()->json(
            $returnVal,
            200,
        ['Content-type'=> 'application/json; charset=utf-8'],
        JSON_UNESCAPED_UNICODE
        );
    }

    public function store(Request $request)
    {
        // instantiate classes
        $oEr = new ErrorClass();
        $oAuth = new Authentication();

        // Check that Object has needed variable need
        if (!empty($request->username) && !empty($request->password)) {
            // Do Authentication Here and return a token
            // Get Security User Based on username
            $SecUser = $this->getSecurityUser($request);
						//var_dump($SecUser);
            if (!empty($SecUser->fwu_key)) {
                // Check Passwords
                if (PasswordUtils::CheckPassword($request->password, $SecUser->fwu_password)) {
                    // Upsert Token
                    $szSql = sprintf("CALL get_access_token('%s');", $SecUser->fwu_key);

                    $authToken = DataUtils::ExecuteScalar($szSql, null);
                    // Upsert Token End
                    $authToken = $authToken[0];
                    // Set oAuth
                    if (!empty($authToken->fws_token)) {
                        $oAuth->token = $authToken->fws_token;
                        $oAuth->status = "Success";
                        $oAuth->user_key = $SecUser->fwu_key;
                        $oAuth->expire_date = date('Y-m-d h:i:s A', strtotime("+2 days"));
                        ;
                    } else {
                        $oEr->ErrorNumber = 1;
                        $oEr->ErrorMessage = "Error during authentication process, Please contact an administrator.";
                        return response()->json(
                        $SecUser,
                        200,
                    ['Content-type'=> 'application/json; charset=utf-8'],
                    JSON_UNESCAPED_UNICODE
									);
                    }
                } else {
                    $oEr->ErrorNumber = 1;
                    $oEr->ErrorMessage = "Login credentials are invalid, please try again";
                    return response()->json(
                    $oEr,
                    200,
                ['Content-type'=> 'application/json; charset=utf-8'],
                JSON_UNESCAPED_UNICODE
                );
                }
            } else {
                $oEr->ErrorNumber = 1;
                $oEr->ErrorMessage = "Login credentials do not exist, please contanct your administrator.";
                return response()->json(
                $oEr,
                200,
            ['Content-type'=> 'application/json; charset=utf-8'],
            JSON_UNESCAPED_UNICODE
            );
            }
        } else {
            $oEr->ErrorNumber = 1;
            $oEr->ErrorMessage = "The json data was badly constructed and does not contain what is needed.";
            return response()->json(
                    $oEr,
                    200,
                ['Content-type'=> 'application/json; charset=utf-8'],
                JSON_UNESCAPED_UNICODE
                );
        }

        return response()->json(
          $oAuth,
          200,
      ['Content-type'=> 'application/json; charset=utf-8'],
      JSON_UNESCAPED_UNICODE
      );
    }

    // Get Sec User function
    private function getSecurityUser($object)
    {
        $szSql = "CALL get_security_user('?');";
        $user = new SecurityUser();
        // Get SecUser From Db
        $result = DataUtils::simpleSelect('fw_user', 'fwu_user_name', $object->username);

        if (!empty($result) && strpos(get_class($result), 'ErrorClass') == 0) {
            $user->fwu_key = $result->fwu_key;
            $user->fwu_user_name = $result->fwu_user_name;
            $user->fwu_password = $result->fwu_password;
        } else {
					$user = $result;
				}

        return $user;
    }
}
