<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

// Models
use App\Http\Models\ErrorClass;
use App\Http\Models\Authentication;
use App\Http\Models\LoginModel;

// Utilities
use App\CustomLib\DataUtils;
use App\CustomLib\PasswordUtils;
use App\CustomLib\AuthUtils;

class LoginController extends BaseController
{
    public function index()
    {
        $returnVal = ['value_1' => '1',
      'value_2' => '2'];
        //
        return response()->json(
            $returnVal,
            200,
        ['Content-type'=> 'application/json; charset=utf-8'],
        JSON_UNESCAPED_UNICODE
        );
    }

    public function store(Request $request)
    {
        // Instantiate ErrorClass
        $oEr = new ErrorClass();
        // Create Response Variable
        $response = "";
        // Grab Token from Request Header and check it against the DB
        $szAuthToken = $request->header("token");
        $authResult = AuthUtils::Authenticate($szAuthToken);
        // If Authenticated then continue with process
        if ($authResult) {
            // Log User In
            if ((!empty($request->usr_email) || !empty($request->usr_user_name)) && !empty($request->usr_password)) {
                // Instantiate LoginModel
                $login = new LoginModel();
                // Select from table
                $result = DataUtils::simpleSelect("co_user", "usr_email", $request->usr_email);
                // Check password and Validate from there
                if (PasswordUtils::CheckPassword($request->usr_username, $result["usr_password"])) {
                    // Set User login at success
                    $login->usr_key = $result->usr_key;
                    $login->usr_first_name = $result->usr_first_name;
                    $login->usr_last_name = $result->usr_last_name;
                    $login->usr_user_name = $result->usr_user_name;
                    $login->usr_email = $result->usr_email;
                    $login->usr_bio = !empty($result->usr_bio)?$result->usr_bio:'';
                    $login->Message = "Logged into account successfully";
                    $login->status = "Success";
                    $response = $login;
                } else {
                    // Invalid Credentials
                    $login->Message = "Invalid credentials";
                    $login->status = "Access Denied";
                    $response = $login;
                }
            } else {
                // Insufficient Parameters
                $login->Message = "Insufficient information provided. Must have an email or username and a password";
                $login->status = "Failed";
                $response = $login;
            }
        } else {
            // Authentication Failed
            $oEr->ErrorNumber = -1;
            $oEr->Message = "Authentication failed, please reauthenticate and try again.";
            $response = $oEr;
        }
        // Return Result
        return response()->json(
                $returnVal,
                200,
            ['Content-type'=> 'application/json; charset=utf-8'],
            JSON_UNESCAPED_UNICODE
            );
    }
}
