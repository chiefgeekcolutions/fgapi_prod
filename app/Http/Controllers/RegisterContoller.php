<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

// Models
use App\Http\Models\ErrorClass;
use App\Http\Models\Authentication;
use App\Http\Models\SecurityUser;

// Utilities
use App\CustomLib\DataUtils;
use App\CustomLib\PasswordUtils;
use App\CustomLib\AuthUtils;

class RegisterController extends BaseController
{
    // insert_application_user
    public function index()
    {
        $returnVal = ['value_1' => '1',
      'value_2' => '2'];
        //
        return response()->json(
          $returnVal,
          200,
      ['Content-type'=> 'application/json; charset=utf-8'],
      JSON_UNESCAPED_UNICODE
      );
    }

    public function store(Request $request)
    {
        $oEr = new ErrorClass();
        $response = "";

        $szAuthToken = $request->header("token");
        $authResult = AuthUtils::Authenticate($szAuthToken);

        if ($authResult) {
            if (!empty($request->usr_user_name) && !empty($request->usr_password)) {
                $userInput = $request->all();
                //print_r($input);
                $response = DataUtils::simpleInsert("co_user", $userInput);
            } else {
                $oEr->ErrorNumber = -1;
                $oEr->Message = "Please Provide atleast a username and password.";
								logError("RegisterController->store", "", "Web API", $oEr);
                $response = $oEr;
            }
        } else {
            $oEr->ErrorNumber = -1;
            $oEr->Message = "Authentication failed, please reauthenticate and try again.";
						logError("RegisterController->store", "", "Web API", $oEr);
            $response = $oEr;
        }

        return response()->json(
                $response,
                200,
            ['Content-type'=> 'application/json; charset=utf-8'],
            JSON_UNESCAPED_UNICODE
            );
    }
}
