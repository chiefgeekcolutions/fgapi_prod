<?php namespace App\Http\Models;

class PedometerModel
{
    public $ped_key;
    public $ped_usr_key;
    public $ped_start_datetime;
    public $ped_end_datetime;
    public $ped_steps_total;
    public $ped_steps_per_hour;
    public $ped_add_date;
    public $ped_add_user;
    public $ped_change_date;
    public $ped_change_user;
    public $ped_delete_flag;
}
