<?php namespace App\Http\Models;

class Authentication
{
    public $user;
    public $user_key;
    public $status;
    public $token;
    public $expire_date;
    public $expire_type;
}
