<?php namespace App\Http\Models;

class LoginModel
{
    public $usr_key;
    public $usr_user_name;
    public $usr_first_name;
    public $usr_last_name;
    public $usr_email;
    public $usr_bio;
    public $status;
    public $message;
}
