<?php namespace App\Http\Models;

class WaterLogModel
{
    public $wtr_key;
    public $wtr_usr_key;
    public $wtr_start_datetime;
    public $wtr_end_datetime;
    public $wtr_gallon_total;
    public $wtr_add_date;
    public $wtr_add_user;
    public $wtr_change_date;
    public $wtr_change_user;
    public $wtr_delete_flag;
}
